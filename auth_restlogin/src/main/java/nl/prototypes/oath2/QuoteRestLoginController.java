package nl.prototypes.oath2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.AllArgsConstructor;
import nl.prototypes.oath2.utils.Quote;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.WebRequest;

import javax.annotation.PostConstruct;
import java.security.Principal;
import java.text.SimpleDateFormat;

@AllArgsConstructor
@RestController
public class QuoteRestLoginController {

    RestTemplate restTemplate;

    ObjectMapper objectMapper;

    @PostConstruct
    public void init(){
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        objectMapper.setDateFormat(sdf);
    }

    @RequestMapping("/user")
    public Principal user(Principal principal) {
        return principal;
    }

    @RequestMapping(value = "/")
    public Quote getQuote(WebRequest webRequest) {
        Quote quote = restTemplate.getForObject(
                "http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
        return quote;
    }

}
