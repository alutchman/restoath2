package nl.prototypes.oath2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;

@Slf4j
@SpringBootApplication
@Import({ ServerSecurityConfig.class })
public class ApplicationRestLogin {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean(name="oath2")
    public PasswordEncoder oauthClientPasswordEncoder() {
        return new BCryptPasswordEncoder(4);
    }
/*

    @Primary
    @Bean(name="oath2")
    public PasswordEncoder oauthClientPasswordEncoder() {
        return new BCryptPasswordEncoder(4);
    }

    @Bean(name="general")
    public PasswordEncoder userPasswordEncoder() {
        return new BCryptPasswordEncoder(8);
    }*/


    public static void main(String args[]) {
        ConfigurableApplicationContext context = SpringApplication.run(ApplicationRestLogin.class);
        log.info("login with name = 'john' !");
        log.info("login with password = '{}' !", "welkom01");

    }
}
